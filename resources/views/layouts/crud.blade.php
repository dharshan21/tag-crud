
@extends('layouts.app')
@section('content')
<h1>Add New Task</h1>
<hr>
<form action="{{ route('crud.store')}}" method="post">
@csrf
    <div class="form-group">
        <label for="title">Task Title</label>
        <input type="text" class="form-control" id="taskTitle"  name="title">
    </div>
    <div class="form-group">
        <label for="description">Task Description</label>
         <textarea type="text" class="form-control"  name="description" rows="15" cols="135px"></textarea>
     </div> 
     <div class="form-group">
        <label for="tags">Task tag</label>
        <input type="text" class="form-control" id="taskTitle"  name="tags">
    </div>

   
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection