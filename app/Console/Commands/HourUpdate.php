<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Mail;

use App\Crud;

class HourUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minute:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cruds = Crud::all();
        foreach ($cruds as $crud) {
            
            Mail::raw("email send the message", function($message) use ($crud)
            {
                $message->from('prasanthmca1998@gmail.com');
                $message->to($crud->email)->subject('Hour Update');
            });
        }
         $this->info('Successfully');
    }
}
